/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.umg.practicap1.dto;

import java.io.Serializable;

/**
 *
 * @author melvin.noriega@megapaca.com
 */
public class Persona implements Serializable{

    private int edad;
    private String nombre;
    private String direccion;
    private String genero;

    public Persona() {
    }

    public Persona(int edad, String nombre, String direccion, String genero) {
        this.edad = edad;
        this.nombre = nombre;
        this.direccion = direccion;
        this.genero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    
    
}
