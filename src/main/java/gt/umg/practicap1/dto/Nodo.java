/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.umg.practicap1.dto;

/**
 *
 * @author ploks
 */
public class Nodo {
    
    private Persona dato;
    private Nodo izq, der;

    public Nodo(Persona dato, Nodo izq, Nodo der) {
        this.dato = dato;
        this.izq = izq;
        this.der = der;
    }

    public Persona getDato() {
        return dato;
    }

    public void setDato(Persona dato) {
        this.dato = dato;
    }

    public Nodo getIzq() {
        return izq;
    }

    public void setIzq(Nodo izq) {
        this.izq = izq;
    }

    public Nodo getDer() {
        return der;
    }

    public void setDer(Nodo der) {
        this.der = der;
    } 

}
